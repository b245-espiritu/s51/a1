import { Fragment } from "react"
import Banner from "../components/banner"
import Hightlights from "../components/hightlights"


export default function Home(){
    return(
        <Fragment>
            <Banner/>
            <Hightlights/>

           
        </Fragment>
    )
}