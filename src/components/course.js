import { useState } from 'react';
import Card from 'react-bootstrap/Card';
import {  Button} from 'react-bootstrap';

import { Container } from "react-bootstrap"

function Course(props){

    const { id, name, description, price} = props;

    const [enrolledCount, seEnrolledCount] = useState(0);
    const [seatCount, setSeatCount] = useState(30)

    const addEnrolledCount = ()=>{
            if(seatCount > 0){
                seEnrolledCount(previous => previous + 1)
                setSeatCount(previous => previous - 1)
            }
            else{
                alert('No more seat available!')
            }

            
        
    }
   
    return(
       
          
        <Container className="mt-5">
               
                   <Card id={id}>
                       <Card.Body>
                           <Card.Title>{name}</Card.Title>
                           <Card.Subtitle>Description</Card.Subtitle>
                           <Card.Text>{description}</Card.Text>

                           <Card.Subtitle>Price</Card.Subtitle>
                           <Card.Text>{price}</Card.Text>

                           <Card.Subtitle>Current Enrolled</Card.Subtitle>
                           <Card.Text>{enrolledCount}</Card.Text>

                           <Card.Subtitle>Available Seat</Card.Subtitle>
                           <Card.Text>{seatCount}</Card.Text>
                          
                          
                           <Button onClick={addEnrolledCount} variant="primary">Enroll Now</Button>
                       </Card.Body>
                   </Card>
              
           
        </Container>
                

                
        
    )
}

export default Course;