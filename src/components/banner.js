
import { Row, Col, Container, Button} from 'react-bootstrap';

export default function Banner(){
    return(
       <Container className='mt-5'>
            <Row>
                <Col className='text-center'>
                    <h1>Zuitt Coding Bootcamp</h1>
                    <p className='pt-1'>Opportunities for everyone, everywhere</p>
                    <Button className='pt-1'>Enroll Now</Button>
                </Col>
            </Row>
       </Container>
    )
}