import { Fragment } from 'react';
import './App.css';
import AppNavBar from './components/appNavbar';
import Home from './pages/home';
import Courses from './pages/courses';

function App() {
  return (

   <Fragment>
       <AppNavBar/>
      <Home/>
      <Courses/>
   </Fragment>
   
  );
}

export default App;
